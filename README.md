# Replicator
Replicator can manage several Mastodon or Pleroma's bot accounts who will replicate (post to Mastodon/Pleroma) several Twitter accounts's tweets of your choice.  

### Dependencies

-   **Python 3**
-   Postgresql server
-   Several Mastodon or Pleroma's bot accounts  
-   One Twitter user account API KEY and SECRET
-   Your personal Linux or Raspberry box.

### Usage:

Within Python Virtual Environment:

1. Run `pip install -r requirements.txt` to install needed libraries.

2. Run `python db-setup.py` to setup and create new Postgresql database and needed table in it.

3. Run `python twitter-setup.py` to input and save your Twitter's key and access token. You can get your API key and API key secret from [Twitter Developer Platform](https://developer.twitter.com/en/apply/user.html)  

4. Run `python bots-setup.py` to setup your Mastodon or Pleroma's bot accounts access tokens, so many as you wish. Each bot will replicate (post to Mastodon) all new tweets from each configured Twitter's users.

5. Use your favourite scheduling method to set `python replicator.py` to run every minute.  

18.2.2022 **New feature**. Added Pleroma support.  
25.2.2022 **New feature**. Added menu to create, delete or list bots.  
28.3.2022 **New feature**. Added Activation/Deactivation of any single bot.
